<?php

/**
 * @file
 * Administrative page callbacks for the Typage module.
 */

/**
 * Menu callback for the configuration form of the module.
 */
function typage_settings() {
  $form = array();

  $nodetypes = _node_types_build()->types;

  foreach ($nodetypes as $nodetype) {

    $type = drupal_strtolower($nodetype->type);

    //get data for $type from db => inserting it if doens't exist
    $typage = typage_get_type($type);

    $form['general'] = array(
      '#type' => 'fieldset',
      '#title' => t('General'),
      '#collapsible' => FALSE
    );

    $form['general']['typage_show_add_local_task'] = array(
      '#type' => 'checkbox',
      '#title' => t('Show the local task \'Add content\' on the main content page.'),
      '#default_value' => variable_get('typage_show_add_local_task', 0)
    );

    $form['general']['typage_show_all_content_link'] = array(
      '#type' => 'checkbox',
      '#title' => t('Show a link to the general node overview page.'),
      '#default_value' => variable_get('typage_show_all_content_link', 0)
    );

    $form['general']['typage_show_all_content_title'] = array(
      '#type' => 'textfield',
      '#title' => t('Custom title'),
      '#default_value' => variable_get('typage_show_all_content_title', t('All content'))
    );

    $form['type_' . $type] = array(
      '#type' => 'fieldset',
      '#title' => $nodetype->name,
      '#collapsible' => TRUE,
      '#collapsed' => TRUE
    );

    $form['type_' . $type]['typage_' . $type . '_enable'] = array(
      '#type' => 'checkbox',
      '#title' => t('Create page'),
      '#default_value' => $typage->enabled,
      '#attributes' => array(
        'class' => array('typage_checkbox_enable')
      )
    );

    $form['type_' . $type]['typage_' . $type . '_title'] = array(
      '#type' => 'textfield',
      '#title' => t('Custom title'),
      '#description' => t('Give the page a custom title. Leave blank to use the nodetype as title.'),
      '#default_value' => $typage->title,
      '#attributes' => array(
        'class' => array('hide-when-disabled')
      )
    );

    $form['type_' . $type]['typage_' . $type . '_description'] = array(
      '#type' => 'textarea',
      '#title' => t('Description'),
      '#default_value' => $typage->description,
      '#attributes' => array(
        'class' => array('hide-when-disabled')
      )
    );

    $options = array(
      'author' => t('Author'),
      'status' => t('Status'),
      'promoted' => t('Promoted to front page')
    );

    //show all fields for the current nodetype
    $allowed_types = array('image', 'list_boolean', 'taxonomy_term_reference'); //TODO: add more types
    foreach (field_info_instances("node", $typage->type) as $field_name => $info) {
      $field = field_info_field_by_id($info['field_id']);
      if ($field_name != 'body' && in_array($field['type'], $allowed_types)) {
        $options[$field_name] = $info['label'];
      }
    }

    $form['type_' . $type]['typage_' . $type . '_fields'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Data to display in list'),
      '#options' => $options,
      '#default_value' => $typage->fields,
      '#attributes' => array(
        'class' => array('hide-when-disabled')
      )
    );

    $form['type_' . $type]['typage_' . $type . '_redirect'] = array(
      '#type' => 'checkbox',
      '#title' => t('Return to page after adding or editing a node.'),
      '#description' => t('Checking this will override the default behavior of redirecting to the node after editing. Instead you\'ll be redirected back to the Typage overview page.'),
      '#default_value' => $typage->redirect,
      '#attributes' => array(
        'class' => array('hide-when-disabled')
      )
    );

    $form['type_' . $type]['typage_' . $type . '_showfilter'] = array(
      '#type' => 'checkbox',
      '#title' => t('Show filter form on page.'),
      '#default_value' => $typage->showfilter,
      '#attributes' => array(
        'class' => array('hide-when-disabled')
      )
    );
  }


  $form = system_settings_form($form);

  //disable the default submit behavior and add our own submit handler
  $form['#submit'] = array();
  $form['#submit'][] = 'typage_settings_submit';

  drupal_add_js(drupal_get_path('module', 'typage') . '/typage.admin.js');

  return $form;
}

/**
 * Main submit handler for the configuration form.
 */
function typage_settings_submit($form, &$form_state) {
  $values = $form_state['values'];

  variable_set('typage_show_all_content_link', $values['typage_show_all_content_link']);
  variable_set('typage_show_all_content_title', $values['typage_show_all_content_title']);
  variable_set('typage_show_add_local_task', $values['typage_show_add_local_task']);

  $nodetypes = _node_types_build()->types;

  foreach ($nodetypes as $nodetype) {
    $type = drupal_strtolower($nodetype->type);
    $typage = typage_get_type($type);

    if ($values['typage_' . $type . '_enable']) {
      $typage->enabled = TRUE;
    }
    else {
      $typage->enabled = FALSE;
    }

    $typage->fields = array();
    if ($values['typage_' . $type . '_fields']) {
      foreach ($values['typage_' . $type . '_fields'] as $key => $field) {
        if ($field)
          $typage->fields[] = $key;
      }
    }
    if ($values['typage_' . $type . '_title']) {
      $typage->title = $values['typage_' . $type . '_title'];
    }
    else {
      $typage->title = '';
    }
    if ($values['typage_' . $type . '_description']) {
      $typage->description = $values['typage_' . $type . '_description'];
    }
    else {
      $typage->description = '';
    }
    if ($values['typage_' . $type . '_redirect']) {
      $typage->redirect = TRUE;
    }
    else {
      $typage->redirect = FALSE;
    }

    if ($values['typage_' . $type . '_showfilter']) {
      $typage->showfilter = TRUE;
    }
    else {
      $typage->showfilter = FALSE;
    }

    typage_save_type($typage);
  }

  menu_rebuild();
  drupal_set_message(t('The configuration options have been saved.'));
  drupal_set_message(t('Menu has been rebuild.'));
}

/**
 * Menu callback of the content page.
 */
function typage_content() {
  $item = menu_get_item();
  if ($content = system_admin_menu_block($item)) {
    $output = theme('admin_block_content', array('content' => $content));
  }
  else {
    $output = t('You do not have any administrative items.');
  }
  return $output;
}
