<?php

/**
 * @file
 * Handlers to show fields in the node overview grid.
 */

/**
 * Main handler to display a field.
 */
function typage_display_field($node, $field_name) {
  global $language;

  $field = field_info_field($field_name);
  $field_type = $field['type'];
  $field_value = $node->$field_name;

  if (function_exists('typage_display_field_' . $field_type)) {
    return call_user_func('typage_display_field_' . $field_type, $field_value);
  }
  else {
    $value = "";
    if (array_key_exists($language->language, $field_value)) {
      $value = $field_value[$language->language];
    }
    elseif (array_key_exists('und', $field_value)) {
      $value = $field_value['und'];
    }
    return is_array($value) ? "" : $value;
  }
}

/**
 * Displays an image field
 */
function typage_display_field_image($value) {
  global $language;

  $output = "";

  if (array_key_exists($language->language, $value)) {
    $value = $value[$language->language];
  }
  elseif (array_key_exists('und', $value)) {
    $value = $value['und'];
  }

  if ($value) {
    $thumbnail_path = image_style_url('thumbnail', $value[0]['uri']);
    $medium_path = image_style_url('medium', $value[0]['uri']);

    $output .= "<div class='typage_field_image'>";
    $output .= theme('image', array('path' => $thumbnail_path, 'attributes' => array('class' => 'typage_field_image_thumb')));
    $output .= theme('image', array('path' => $medium_path, 'attributes' => array('class' => 'typage_field_image_medium')));
    $output .= "</div>";
  }

  return $output;
}

/**
 * Displays a list_boolean field.
 */
function typage_display_field_list_boolean($value) {
  global $language;

  if (array_key_exists($language->language, $value)) {
    $value = $value[$language->language];
  }
  elseif (array_key_exists('und', $value)) {
    $value = $value['und'];
  }

  if ($value) {
    $value = $value[0]['value'] == "1";
  }
  else {
    $value = FALSE;
  }

  return $value ? t("Yes") : "";
}

/**
 * Displays a taxonomy_term_reference field.
 */
function typage_display_field_taxonomy_term_reference($value) {
  global $language;

  if (array_key_exists($language->language, $value)) {
    $value = $value[$language->language];
  }
  elseif (array_key_exists('und', $value)) {
    $value = $value['und'];
  }
  else {
    $value = "";
  }

  if ($value) {
    if (is_array($value)) {
      $list = $value;
      $value = "";
      foreach ($list as $i => $term) {
        $tid = $term['tid'];
        $term = taxonomy_term_load($tid);
        $value .= ($i ? ", " : "") . $term->name;
      }
    }
  }

  return $value;
}
