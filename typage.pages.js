jQuery(function($) {

	$('.typage_field_image').each(function(){
		var $thumb = $('.typage_field_image_thumb', this);
		var $medium = $('.typage_field_image_medium', this); 
		$thumb.mouseover(function(){
			$medium.show();			
		});
		$medium.mouseout(function(){
			$medium.hide();
		});
	});	
});