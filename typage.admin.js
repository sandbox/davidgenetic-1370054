jQuery(function($) {

	var $typage_form = $('form#typage-settings');
	
	//on load
	$('.typage_checkbox_enable', $typage_form).each(function() {
		if (this.checked)
			$(this).parents('fieldset').find('.hide-when-disabled').parents('.form-item').show();
		else
			$(this).parents('fieldset').find('.hide-when-disabled').parents('.form-item').hide();
	});

	//on click
	$('.typage_checkbox_enable', $typage_form).click(function() {
		if (this.checked)
			$(this).parents('fieldset').find('.hide-when-disabled').parents('.form-item').show();
		else
			$(this).parents('fieldset').find('.hide-when-disabled').parents('.form-item').hide();
	});
	
});
