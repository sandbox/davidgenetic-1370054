<?php

/**
 * @file
 * The Node-overview pages.
 */


/**
 * Menu callback: content administration.
 */
function typage_page($form, $form_state, $type) {

  //make several functions from the node module available for use
  module_load_include('inc', 'node', 'node.admin');
  module_load_include('inc', 'typage', 'typage.fields');

  if (isset($form_state['values']['operation']) && $form_state['values']['operation'] == 'delete') {
    return typage_multiple_delete_confirm($form_state, array_filter($form_state['values']['nodes']), $type);
  }

  $typage = typage_get_type($type);

  if ($typage->showfilter) {
    $form['filter'] = typage_filter_form($typage->type);
    $form['#submit'][] = 'typage_filter_form_submit';
  }
  $form['admin']  = typage_page_form($typage);

  drupal_add_js(drupal_get_path('module', 'typage') . '/typage.pages.js');
  drupal_add_css(drupal_get_path('module', 'typage') . '/typage.pages.css');

  return $form;
}

/**
 * Builds the actual overview
 */
function typage_page_form($typage) {

  $admin_access = user_access('access typage page');
  $uses_weight = typage_weight_available($typage->type);
  $type_obj = node_type_load($typage->type);

  $can_create = user_access("create {$typage->type} content");

  if ($can_create) {

    $form['actions'] = array(
      '#type' => 'fieldset'
    );

    $form['actions']['add'] = array(
      '#type' => 'submit',
      '#value' => t('Add new'),
      '#submit' => array('typage_goto_add')
    );
  }

  //in the Weight module there is an option to create a 'weight changer'-view (requires the Views module)
  //here we try to find one for the current type and add a button to access it if it exists
  if ($uses_weight) {
    $view_url = typage_get_weight_changer_view_url($typage->type);
    if ($view_url) {
      $form['actions']['weight_changer_url'] = array(
        '#type' => 'hidden',
        '#value' => $view_url
      );
      $form['actions']['weights'] = array(
        //'#markup' => "<input type='button' class='form-submit button-weight' onclick='window.location = \"/$view_url\"; return false;' value='Bepaal volgorde' />"
        '#type' => 'submit',
        '#value' => t('Change weights'),
        '#submit' => array('typage_goto_weight_changer')
      );
    }
  }

  $form['content_type'] = array(
    '#type' => 'hidden',
    '#value' => $typage->type
  );

  $form['redirect'] = array(
    '#type' => 'hidden',
    '#value' => $typage->redirect
  );

  //create options fieldset
  $form['options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Update options'),
    '#prefix' => '<div class="container-inline">',
    '#suffix' => '</div>',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $options = array();
  foreach (module_invoke_all('node_operations') as $operation => $array) {
    if ($operation == 'reset_weights' && !$uses_weight)
      continue;
    $label = $array['label'];
    if ($type_obj)
      $label = str_replace(t('content'), drupal_strtolower($type_obj->name), $array['label']);
    $options[$operation] = $label;
  }
  $form['options']['operation'] = array(
    '#type' => 'select',
    '#options' => $options,
    '#default_value' => 'approve'
  );
  $form['options']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
    '#submit' => array('node_admin_nodes_submit'),
    '#file' => '/modules/node/node.admin.inc'
  );

  //if Weight module available for $type, remove sticky operations if enabled for current type
  if ($uses_weight) {
    unset($form['options']['operation']['#options']['sticky']);
    unset($form['options']['operation']['#options']['unsticky']);
  }

  // Enable language column if locale is enabled or if we have any node with language
  $multilanguage = (module_exists('translation') || db_query_range("SELECT 1 FROM {node} WHERE language <> :language", 0, 1, array(':language' => LANGUAGE_NONE))->fetchField());

  // Build the sortable table header.
  $header = array();
  $header['title'] = array('data' => t('Title'), 'field' => 'n.title');

  //fields to display
  foreach (field_info_instances("node", $typage->type) as $field_name => $info) {
    if (array_search($field_name, $typage->fields) !== FALSE)
      $header[$field_name] = array('data' => $info['label']);
  }

  if (array_search('promoted', $typage->fields) !== FALSE)
    $header['promoted'] = array('data' => t('Promoted to front page'), 'field' => 'n.promote');
  if (array_search('author', $typage->fields) !== FALSE)
    $header['author'] = array('data' => t('Author'));
  if (array_search('status', $typage->fields) !== FALSE)
    $header['status'] = array('data' => t('Status'), 'field' => 'n.status');
  $header['changed'] = array('data' => t('Updated'), 'field' => 'n.changed', 'sort' => 'desc');

  if ($multilanguage) {
    $header['language'] = array('data' => t('Language'), 'field' => 'n.language');
  }

  $header['operations'] = array('data' => t('Operations'));

  $query = db_select('node', 'n')->extend('PagerDefault')->extend('TableSort')->condition('n.type', $typage->type);
    typage_build_filter_query($query, $typage->type);

    $nids = $query
      ->fields('n', array('nid'))
      ->limit(50)
      ->orderByHeader($header)
      ->execute()
    ->fetchCol();
  $nodes = node_load_multiple($nids);

  $languages = language_list();
  $destination = drupal_get_destination();
  $options = array();
  foreach ($nodes as $node) {
    $l_options = $node->language != LANGUAGE_NONE && isset($languages[$node->language]) ? array('language' => $languages[$node->language]) : array();
      $options[$node->nid]['title'] = array(
          'data' => array(
              '#type' => 'link',
              '#title' => check_plain($node->title),
              '#href' => 'node/' . $node->nid,
              '#options' => $l_options,
              '#suffix' => ' ' . theme('mark', array('type' => node_mark($node->nid, $node->changed))),
          ),
      );
      foreach (field_info_instances("node", $typage->type) as $field_name => $info) {
      if (array_search($field_name, $typage->fields) !== FALSE)
        $options[$node->nid][$field_name] = typage_display_field($node, $field_name);
    }
    if (array_search('promoted', $typage->fields) !== FALSE)
        $options[$node->nid]['promoted'] = $node->promote ? t('Yes') : "";
      if (array_search('author', $typage->fields) !== FALSE)
        $options[$node->nid]['author'] = theme('username', array('account' => $node));
      if (array_search('status', $typage->fields) !== FALSE)
        $options[$node->nid]['status'] = $node->status ? t('published') : t('not published');
      $options[$node->nid]['changed'] = format_date($node->changed, 'short');

      if ($multilanguage) {
        if ($node->language == LANGUAGE_NONE || isset($languages[$node->language])) {
            $options[$node->nid]['language'] = $node->language == LANGUAGE_NONE ? t('Language neutral') : t($languages[$node->language]->name);
          }
          else {
            $options[$node->nid]['language'] = t('Undefined language (@langcode)', array('@langcode' => $node->language));
          }
      }

      // Build a list of all the accessible operations for the current node.
      $operations = array();
      if (node_access('update', $node)) {
        $operations['edit'] = array(
            'title' => t('edit'),
            'href' => 'node/' . $node->nid . '/edit',
            'query' => $destination,
          );
      }
      if (node_access('delete', $node)) {
        $operations['delete'] = array(
            'title' => t('delete'),
            'href' => 'node/' . $node->nid . '/delete',
            'query' => $destination,
          );
      }
      $options[$node->nid]['operations'] = array();
      if (count($operations) > 1) {
        // Render an unordered list of operations links.
        $options[$node->nid]['operations'] = array(
            'data' => array(
                '#theme' => 'links__node_operations',
                '#links' => $operations,
                '#attributes' => array('class' => array('links', 'inline')),
            ),
          );
      }
      elseif (!empty($operations)) {
        // Render the first and only operation as a link.
          $link = reset($operations);
          $options[$node->nid]['operations'] = array(
            'data' => array(
                '#type' => 'link',
                '#title' => check_plain($link['title']),
                '#href' => $link['href'],
                '#options' => array('query' => $link['query']),
            ),
          );
      }
  }

  // Only use a tableselect when the current user is able to perform any
  // operations.
  if ($admin_access) {
    $form['nodes'] = array(
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $options,
      '#empty' => t('No content available.')
    );
  }
  // Otherwise, use a simple table.
  else {
    $form['nodes'] = array(
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $options,
      '#empty' => t('No content available.'),
    );
  }

  $form['pager'] = array('#markup' => theme('pager'));

  return $form;
}

/**
 * Build query for node administration filters based on session. (copy from Node module)
 */
function typage_build_filter_query(SelectQueryInterface $query, $type) {
  // Build query
  $filter_data = isset($_SESSION['typage_' . $type . '_filter']) ? $_SESSION['typage_' . $type . '_filter'] : array();
  foreach ($filter_data as $index => $filter) {
    list($key, $value) = $filter;
    switch ($key) {
      case 'term':
        $alias = $query->join('taxonomy_index', 'ti', "n.nid = %alias.nid");
        $query->condition($alias . '.tid', $value);
        break;
      case 'status':
        // Note: no exploitable hole as $key/$value have already been checked when submitted
        list($key, $value) = explode('-', $value, 2);
      case 'type':
      case 'language':
        $query->condition('n.' . $key, $value);
        break;
    }
  }
}

/**
 * List node administration filters that can be applied. (copy from Node module)
 */
function typage_filters() {
  // Regular filters
  $filters['status'] = array(
    'title' => t('status'),
    'options' => array(
      '[any]' => t('any'),
      'status-1' => t('published'),
      'status-0' => t('not published'),
      'promote-1' => t('promoted'),
      'promote-0' => t('not promoted'),
      'sticky-1' => t('sticky'),
      'sticky-0' => t('not sticky'),
    ),
  );
  // Include translation states if we have this module enabled
  if (module_exists('translation')) {
    $filters['status']['options'] += array(
      'translate-0' => t('Up to date translation'),
      'translate-1' => t('Outdated translation'),
    );
  }

  // Language filter if there is a list of languages
  if ($languages = module_invoke('locale', 'language_list')) {
    $languages = array(LANGUAGE_NONE => t('Language neutral')) + $languages;
    $filters['language'] = array(
      'title' => t('language'),
      'options' => array(
        '[any]' => t('any'),
      ) + $languages,
    );
  }
  return $filters;
}

/**
 * Return form for node administration filters. (copy from Node module)
 */
function typage_filter_form($type) {
  $session = isset($_SESSION['typage_' . $type . '_filter']) ? $_SESSION['typage_' . $type . '_filter'] : array();
  $filters = typage_filters();

  $i = 0;
  $form['filters'] = array(
    '#type' => 'fieldset',
    '#title' => t('Show only items where'),
    '#theme' => 'exposed_filters__node',
  );
  foreach ($session as $filter) {
    list($type, $value) = $filter;
    if ($type == 'term') {
      // Load term name from DB rather than search and parse options array.
      $value = module_invoke('taxonomy', 'term_load', $value);
      $value = $value->name;
    }
    elseif ($type == 'language') {
      $value = $value == LANGUAGE_NONE ? t('Language neutral') : module_invoke('locale', 'language_name', $value);
    }
    else {
      $value = $filters[$type]['options'][$value];
    }
    $t_args = array('%property' => $filters[$type]['title'], '%value' => $value);
    if ($i++) {
      $form['filters']['current'][] = array('#markup' => t('and where %property is %value', $t_args));
    }
    else {
      $form['filters']['current'][] = array('#markup' => t('where %property is %value', $t_args));
    }
    if (in_array($type, array('type', 'language'))) {
      // Remove the option if it is already being filtered on.
      unset($filters[$type]);
    }
  }

  $form['filters']['status'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('clearfix')),
    '#prefix' => ($i ? '<div class="additional-filters">' . t('and where') . '</div>' : ''),
  );
  $form['filters']['status']['filters'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('filters')),
  );
  foreach ($filters as $key => $filter) {
    $form['filters']['status']['filters'][$key] = array(
      '#type' => 'select',
      '#options' => $filter['options'],
      '#title' => check_plain($filter['title']),
      '#default_value' => '[any]',
    );
  }

  $form['filters']['status']['actions'] = array(
    '#type' => 'actions',
    '#attributes' => array('class' => array('container-inline')),
  );
  $form['filters']['status']['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => count($session) ? t('Refine') : t('Filter'),
  );
  if (count($session)) {
    $form['filters']['status']['actions']['undo'] = array('#type' => 'submit', '#value' => t('Undo'));
    $form['filters']['status']['actions']['reset'] = array('#type' => 'submit', '#value' => t('Reset'));
  }

  drupal_add_js('misc/form.js');

  return $form;
}

/**
 * Process result from node administration filter form.
 */
function typage_filter_form_submit($form, &$form_state) {
  $type = $form['admin']['content_type']['#value'];
  $filters = typage_filters();
  switch ($form_state['values']['op']) {
    case t('Filter'):
    case t('Refine'):
      // Apply every filter that has a choice selected other than 'any'.
      foreach ($filters as $filter => $options) {
        if (isset($form_state['values'][$filter]) && $form_state['values'][$filter] != '[any]') {
          // Flatten the options array to accommodate hierarchical/nested options.
          $flat_options = form_options_flatten($filters[$filter]['options']);
          // Only accept valid selections offered on the dropdown, block bad input.
          if (isset($flat_options[$form_state['values'][$filter]])) {
            $_SESSION['typage_' . $type . '_filter'][] = array($filter, $form_state['values'][$filter]);
          }
        }
      }
      break;
    case t('Undo'):
      array_pop($_SESSION['typage_' . $type . '_filter']);
      break;
    case t('Reset'):
      $_SESSION['typage_' . $type . '_filter'] = array();
      break;
  }
}

/**
 * Simple submit handler for the 'Add' button.
 */
function typage_goto_add($form, &$form_state) {
  $type = $form['admin']['content_type']['#value'];
  $type = str_replace("_", "-", $type);
  $options = array();
  if ($form['admin']['redirect']['#value']) {
    $options = array(
      'query' => array(
        'destination' => "admin/content/$type"
    ));
  }
  drupal_goto('node/add/' . $type, $options);
}

/**
 * Simple submit handler for the 'Reorder' button.
 */
function typage_goto_weight_changer($form, &$form_state) {
  $url = $form['admin']['actions']['weight_changer_url']['#value'];
  drupal_goto($url);
}

/**
 * Confirm page before deleting multiple nodes.
 */
function typage_multiple_delete_confirm(&$form_state, $nodes, $type) {
  $form['nodes'] = array('#prefix' => '<ul>', '#suffix' => '</ul>', '#tree' => TRUE);
  // array_filter returns only elements with TRUE values
  foreach ($nodes as $nid => $value) {
    $title = db_result(db_query('SELECT title FROM {node} WHERE nid = %d', $nid));
    $form['nodes'][$nid] = array(
      '#type' => 'hidden',
      '#value' => $nid,
      '#prefix' => '<li>',
      '#suffix' => check_plain($title) . "</li>\n",
    );
  }
  $form['operation'] = array('#type' => 'hidden', '#value' => 'delete');
  $form['#submit'][] = 'typage_multiple_delete_confirm_submit';
  $form['#redirect'] = 'admin/content/' . $type;
  return confirm_form($form,
                      t('Are you sure you want to delete these items?'),
                      'admin/content/node', t('This action cannot be undone.'),
                      t('Delete all'), t('Cancel'));
}

/**
 * Submit handler for the confirm page for deleting multiple nodes.
 */
function typage_multiple_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    foreach ($form_state['values']['nodes'] as $nid => $value) {
      node_delete($nid);
    }
    drupal_set_message(t('The items have been deleted.'));
  }
  //$form_state['redirect'] = 'admin/content/'.$type;
  return;
}
